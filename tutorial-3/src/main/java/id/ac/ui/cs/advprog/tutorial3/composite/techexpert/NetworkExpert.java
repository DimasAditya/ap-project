package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class NetworkExpert extends Employees {
    //TODO Implement
    public NetworkExpert(String name, double salary) {
        this.role = "Network Expert";
        this.name = name;
        this.salary = salary;
        if (salary < 50000.00) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
