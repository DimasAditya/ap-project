package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class MainCompany {
    private static Company company;
    private static Ceo A;
    private static Cto B;
	
    private static BackendProgrammer C;
    private static BackendProgrammer D;
    private static FrontendProgrammer E;
    private static FrontendProgrammer F;
	
    private static UiUxDesigner G;
    private static NetworkExpert H;
    private static SecurityExpert I;

    public static void main(String[] args) {
        company = new Company();

        A = new Ceo("A", 500000.00);
        company.addEmployee(A);
        B = new Cto("B", 320000.00);
        company.addEmployee(zorro);

        C = new BackendProgrammer("C", 94000.00);
        company.addEmployee(C);
        D = new BackendProgrammer("D", 200000.00);
        company.addEmployee(D);

        E = new FrontendProgrammer("E",66000.00);
        company.addEmployee(E);
        F = new FrontendProgrammer("F", 130000.00);
        company.addEmployee(F);

        G = new UiUxDesigner("G", 177000.00);
        company.addEmployee(sanji);
        H = new NetworkExpert("H", 83000.00);
        company.addEmployee(brook);
        I = new SecurityExpert("I", 70000.00);
        company.addEmployee(chopper);

        System.out.println("Active employees :");
        for (Employees employee : company.employeesList) {
            System.out.print(employee.getName() + " is a(n) " + employee.role);
            System.out.println(" and make $" + employee.salary + " a month.");
        }

    }
}