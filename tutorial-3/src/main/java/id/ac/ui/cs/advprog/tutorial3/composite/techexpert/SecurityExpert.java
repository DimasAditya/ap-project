package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class SecurityExpert extends Employees {
    //TODO Implement
    public SecurityExpert(String name, double salary) {
        this.role = "Security Expert";
        this.name = name;
        this.salary = salary;
        if (salary < 70000.00) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
