package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.NoCrustSandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThinBunBurger;

import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChickenMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Lettuce;

public class BurgerPlace {

    public static void main(String[] args) {
        Food sandwich1 = new CrustySandwich();
        sandwich1 = new ChickenMeat(sandwich1);
        System.out.println(sandwich1.getDescription() + " $ " + sandwich1.cost());

        Food sandwich2 = new NoCrustSandwich();
        sandwich2 = new BeefMeat(sandwich2);
        sandwich2 = new Cheese(sandwich2);
        System.out.println(sandwich2.getDescription() + " $ " + sandwich2.cost());

        Food burger1 = new ThinBunBurger();
        burger1 = new BeefMeat(burger1);
        burger1 = new Cheese(burger1);
        burger1 = new Lettuce(burger1);
        System.out.println(burger1.getDescription() + " $ " + burger1.cost());

        Food burger2 = new ThickBunBurger();
        burger2 = new ChickenMeat(burger2);
        burger2 = new Cheese(burger2);
        burger2 = new Lettuce(burger2);
        System.out.println(burger2.getDescription() + " $ " + burger2.cost());
    }
}


