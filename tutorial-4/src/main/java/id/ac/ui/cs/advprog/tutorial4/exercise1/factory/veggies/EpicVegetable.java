package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class EpicVegetable implements Veggies {

    public String toString() {
        return "Epic vegetable";
    }
}
