package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GarlicTest {
    private Veggies garlicClass;

    @Before
    public void setUp() {
        garlicClass = new Garlic();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Garlic", garlicClass.toString());
    }
}
