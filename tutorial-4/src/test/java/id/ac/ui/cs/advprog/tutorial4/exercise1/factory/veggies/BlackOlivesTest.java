package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BlackOlivesTest {

    private Veggies blackOlivesClass;

    @Before
    public void setUp() {
        blackOlivesClass = new BlackOlives();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Black Olives", blackOlivesClass.toString());
    }
}
