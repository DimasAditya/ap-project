package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class SauceTest {

    private Class<?> sauceInterfaceClass;

    @Before
    public void setUp() throws Exception {
        sauceInterfaceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce");
    }

    @Test
    public void testSauceIsAPublicInterface() {
        int classModifiers = sauceInterfaceClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testSauceHasToStringMethod() throws Exception {
        Method toString = sauceInterfaceClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
    }
}
