package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SpinachTest {
    private Veggies spinachClass;

    @Before
    public void setUp() {
        spinachClass = new Spinach();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Spinach", spinachClass.toString());
    }
}
