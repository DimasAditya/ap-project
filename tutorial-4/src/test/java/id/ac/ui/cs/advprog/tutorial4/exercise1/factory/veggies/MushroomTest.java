package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MushroomTest {
    private Veggies mushroomClass;

    @Before
    public void setUp() {
        mushroomClass = new Mushroom();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Mushrooms", mushroomClass.toString());
    }
}
