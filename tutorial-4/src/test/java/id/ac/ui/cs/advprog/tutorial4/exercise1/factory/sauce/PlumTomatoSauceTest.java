package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

public class PlumTomatoSauceTest {

    private Class<?> plumTomatoSauceClass;

    @Before
    public void setUp() throws Exception {
        plumTomatoSauceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce");
    }

    @Test
    public void testPlumTomatoSauceSauceIsASauce() {
        Collection<Type> classInterfaces = Arrays.asList(plumTomatoSauceClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce"))
        );
    }

    @Test
    public void testPlumTomatoSauceSauceOverridetoStringMethod() throws Exception {
        Method toString = plumTomatoSauceClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMethodToString() {
        Sauce concretePlumTomatoSauce = new PlumTomatoSauce();
        assertEquals("Tomato sauce with plum tomatoes", concretePlumTomatoSauce.toString());
    }
}
