package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class VeggiesTest {

    private Class<?> veggiesInterfaceClass;

    @Before
    public void setUp() throws Exception {
        veggiesInterfaceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies");
    }

    @Test
    public void testVeggiesIsAPublicInterface() {
        int classModifiers = veggiesInterfaceClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testVeggiesHasToStringMethod() throws Exception {
        Method toString = veggiesInterfaceClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
    }
}
