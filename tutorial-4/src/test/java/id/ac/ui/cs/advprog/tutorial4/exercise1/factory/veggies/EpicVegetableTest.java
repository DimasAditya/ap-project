package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EpicVegetableTest {
    private Veggies epicVegetableClass;

    @Before
    public void setUp() {
        epicVegetableClass = new EpicVegetable();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Epic vegetable", epicVegetableClass.toString());
    }
}
