package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

public class EpicSauceTest {

    private Class<?> epicSauceClass;

    @Before
    public void setUp() throws Exception {
        epicSauceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.EpicSauce");
    }

    @Test
    public void testEpicSauceIsASauce() {
        Collection<Type> classInterfaces = Arrays.asList(epicSauceClass.getInterfaces());

        assertTrue(classInterfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce"))
        );
    }

    @Test
    public void testEpicSauceOverridetoStringMethod() throws Exception {
        Method toString = epicSauceClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMethodToString() {
        Sauce concreteEpicSauce = new EpicSauce();
        assertEquals("Epic sauce", concreteEpicSauce.toString());
    }
}
