package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends id.ac.ui.cs.advprog.tutorial1.strategy.Duck {
    // TODO Complete me!
    public ModelDuck() {
        flyBehavior = new id.ac.ui.cs.advprog.tutorial1.strategy.FlyNoWay();
        quackBehavior = new id.ac.ui.cs.advprog.tutorial1.strategy.Quack();
    }

    public void display() {
        System.out.println("ini model duck");
    }
}
